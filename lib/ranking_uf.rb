require_relative 'ufs_id'
require_relative 'censo'
require_relative 'ufs_id_search'
require_relative 'search_by_sex'
def ranking_uf
puts "Ranking de nomes por UF"
puts "Insira a UF desejada"
uf_sigla = gets.chomp.upcase
uf_hash = set_uf_hash(uf_sigla)

if uf_hash.nil?
  puts "Sigla inválida, tente novamente"
  return menu
end
puts "==================================================="
puts "Ranking geral de nomes (#{uf_hash["nome"]})\n"
uf_ranking_table(uf_hash["id"])
puts "==================================================="
puts "Ranking masculino (#{uf_hash["nome"]})\n"
uf_male_ranking_table(uf_hash["id"])
puts "==================================================="
puts "Ranking feminino (#{uf_hash["nome"]})\n"
uf_female_ranking_table(uf_hash["id"])
puts "==================================================="
end

def generate_uf_ranking(url)
  resp = RestClient.get "#{url}"
  resp_parsed_body = JSON.parse(resp.body)
  ranking_array = resp_parsed_body[0]["res"][0..9]
  ranking_array.each do |name_freq|
    puts "#{name_freq["ranking"]}º  #{name_freq["nome"]} com #{name_freq["frequencia"]} ocorrências."
  end
end
