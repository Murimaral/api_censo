require 'rest-client'
require 'json'

def uf_male_ranking_table(uf_id)
    url = "https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{uf_id}&sexo=M"
    generate_uf_ranking(url)
end
def uf_female_ranking_table(uf_id)
    url = "https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{uf_id}&sexo=F"
    generate_uf_ranking(url)
end
