require 'rest-client'
require 'json'


def uf_ranking_table(uf_id)
    url = "https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{uf_id}"
    generate_uf_ranking(url)
end