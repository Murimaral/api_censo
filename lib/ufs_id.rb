require 'rest-client'
require 'json'

def set_uf_hash(uf_short)
    url = 'https://servicodados.ibge.gov.br/api/v1/localidades/estados'
    resp = RestClient.get "#{url}"
    ufs_list = JSON.parse(resp.body)
    uf_hash = ufs_list.find{ |uf| uf["sigla"] == uf_short }
    #hash sai em branco ao usar o find

end