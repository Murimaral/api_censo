FROM Ruby:2.6.6

RUN apt-get update
RUN mkdir api_censo

WORKDIR /api_censo
RUN gem install bundler:2.1.4
ADD Gemfile* /api_censo/
RUN bundle install
ADD . /api_censo
