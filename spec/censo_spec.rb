require 'spec_helper'
require 'censo'

describe 'API Census' do
  context 'name query by UF' do
    it 'successfully' do
      json_content = File.read('spec/support/get_names_uf.json')
      faraday_response = double('names', status: 200, body: json_content)

      allow(Faraday).to receive(:get).with('https://www.ibge.gov.br/censo2010/apps/nomes/#/ranking',
                                            { uf: 'ES' })
                                    .and_return(faraday_response)

      consulta = Censo.new.uf_consult('ES')
      expect(consulta).to include('Maria 120.000 pessoas')
      expect(consulta).to include('Joao 110.000 pessoas')

    end
  end
end